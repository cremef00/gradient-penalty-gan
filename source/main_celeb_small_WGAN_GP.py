from sys import path_importer_cache
import tensorflow as tf # tensorflow
from keras import layers, optimizers, datasets, callbacks, losses, initializers, Input, Model # high level api for tensorflow+
from keras.optimizers import Adam, RMSprop, SGD
from keras.constraints import Constraint
import keras.backend as K
import numpy as np # numpy for array manipulation
import matplotlib.pyplot as plt # for plotting
from PIL import Image
import os

learning_rate_dis = 5e-5
learning_rate_gen = 5e-5
batch_size = 64
epochs = 100
seed_size = 100

data_path = os.path.join("F:", "uniss22", "seminar", "data")
checkpoint_path = os.path.join("F:", "uniss22", "seminar", "checkpoints")

files = os.listdir(os.path.join(data_path, "celeba"))

limit = len(files)
load_images = True

data_images = np.empty((limit, 218, 178, 3), dtype=np.uint8)

if load_images:
    data_images = np.load(os.path.join(data_path, "data_celeba_images_{}.npy".format(limit)))
else:
    for i in range(limit):
        data_images[i] = np.asarray(Image.open(os.path.join(data_path, "celeba", files[i])))

        print("{:.2f}%".format(100*i/len(files)), end='\r')

    np.save(os.path.join(data_path, "data_celeba_images_{}.npy".format(limit)), data_images)

# reduce image size to 89x89
x_train = data_images[:, ::2, ::2]
x_train = x_train[:, 10:99, :]

x_train = x_train.astype(np.float32)


# scale data between 0 - 1
x_train /= 255 / 2
x_train -= 1

print("shape of x_train and x_test:")
print(x_train.shape)

def critic_loss(y_true, y_pred):
    factors = y_true * 2 - 1
    return -2 * tf.reduce_mean(y_pred[:, :1] * factors) # [0.8 0.7 0.9 -0.1 -0.2 -0.1] [1 1 1 -1 -1 -1]

def critic_loss_gp(y_true, y_pred):
    factors = y_true * 2 - 1
    return -2 * tf.reduce_mean(y_pred[:, :1] * factors) + tf.reduce_mean(y_pred[:, 1:])

def gen_loss(y_true, y_pred):
    return - tf.reduce_mean(y_pred[:, :1])

def custom_accuracy(y_true, y_pred):
    tmp = K.abs(y_true-y_pred[:, :1])
    return K.mean(1-K.cast(K.greater(tmp,0.5),dtype=float))

# hyperparameter for strength of gradient penalty
gp_factor = 3

# needed for the gradient penalty part in keras
tf.compat.v1.disable_eager_execution()

# discriminator model
dis_input = layers.Input(shape=(89, 89, 3))
dis_conv = layers.Conv2D(32, 5, 2, activation="swish")(dis_input)
dis_conv = layers.Conv2D(64, 4, 2, activation="swish")(dis_conv)
dis_conv = layers.Conv2D(128, 4, 2, activation="swish")(dis_conv)
dis_conv = layers.Conv2D(256, 4, 2, activation="swish")(dis_conv)
dis_conv = layers.Flatten()(dis_conv)
dis_dense = layers.Dense(128, activation="swish")(dis_conv)
dis_pred = layers.Dense(1, activation="sigmoid")(dis_dense) # output (real or fake?)

# gradient penalty term
dis_gp = tf.square(1 - tf.sqrt(tf.reduce_sum(tf.square(tf.gradients(dis_pred, dis_input)[0]), axis=(1, 2)) + 1e-10))

dis_output = tf.concat([dis_pred, dis_gp * gp_factor], axis=1)

dis_model = Model(dis_input, dis_output)
dis_model.summary()

dis_model.compile(optimizer=Adam(learning_rate=learning_rate_dis), loss=critic_loss_gp, metrics=[custom_accuracy, critic_loss])

# generator model
gen_input = layers.Input(shape=(seed_size))
gen_dense = layers.Dense(3*3*512, use_bias=False)(gen_input)
gen_reshape = layers.Reshape((3, 3, 512))(gen_dense)
gen_conv = layers.Conv2DTranspose(512, 3, 2, activation="swish", padding="same")(gen_reshape)
gen_conv = layers.Conv2DTranspose(256, 3, 2, activation="swish", padding="same")(gen_conv)
gen_conv = layers.Conv2DTranspose(128, 4, 2, activation="swish", padding="same")(gen_conv)
gen_conv = layers.Conv2DTranspose(64, 4, 2, activation="swish", padding="same")(gen_conv)
gen_conv = layers.Conv2DTranspose(32, 5, 2, activation="swish", padding="same")(gen_conv)
gen_conv = layers.Conv2D(32, 5, activation="swish", padding="same")(gen_conv)
gen_output = layers.Conv2D(3, 3, activation="tanh", padding="same")(gen_conv)
gen_output = gen_output[:, :89, :89]

gen_model = Model(gen_input, gen_output)
gen_model.summary()

dis_model.trainable = False # freezes the discriminators weights for the upcoming combined model (does not have any effect on the already compiled discriminator model)

# combined model (generator and discriminator concatenated)
com_input = layers.Input(shape=(seed_size))
com_generator_image = gen_model(com_input)
com_discriminator_output = dis_model(com_generator_image)
com_model = Model(com_input, com_discriminator_output)

com_model.compile(optimizer=RMSprop(learning_rate=learning_rate_gen), loss=gen_loss, metrics=[custom_accuracy])

indicies = np.arange(len(x_train)) # datapoints we want to train with

recent_loss_com = 0
recent_loss_dis = 0

recent_accuracy_com = 0
recent_accuracy_dis = 0

n_critic = 5 # how many steps the critic makes each time before the generator trains
verbose_steps = 128 # how often to show plots and make saves
fig = plt.figure() # for plotting later on

run_folder = "celeba_gp_r3"

try:
    os.mkdir(os.path.join(checkpoint_path, run_folder))
    os.mkdir(os.path.join(data_path, run_folder))
except:
    print("Folder already exists.")

loss_hist = []
for m in range(epochs):
    print("epoch", m)
    counter = 0
    dis_counter = 0
    com_counter = 0
    np.random.shuffle(indicies) # shuffle indicies before each epoch
    for i in range(0, len(indicies), batch_size):
        counter += 1

        take_n = min(batch_size, len(indicies) - i) # take the next n datapoints
        batch_indicies = indicies[i:i+take_n] # image indicies to train with in this batch

        seed = np.random.normal(0, 1, (take_n, seed_size)) # sample a batch of seeds from a normal distribution

        generated_images = gen_model.predict(seed) # let the model generate a batch of images

        train_dis = dis_model.train_on_batch(np.concatenate([x_train[batch_indicies], generated_images]), np.concatenate([np.ones(take_n), np.zeros(take_n)])) # train the discriminator
        print("train loss: {:.8f}".format(train_dis[2]), end='\r')
        dis_counter += 1

        recent_loss_dis += train_dis[2]
        recent_accuracy_dis += train_dis[1]

        loss_hist.append(train_dis[2])

        # This has the effect that the discriminator performs n_critic times more training steps (like described in the paper)
        if counter % n_critic == 0:
            train_com = com_model.train_on_batch(seed, np.ones(take_n)) # train the generator
            com_counter += 1
            recent_loss_com += train_com[0]
            recent_accuracy_com += train_com[1]

        if counter % verbose_steps == 0: # show progress after every 20th epoch

            print(f"images seen in epoch {m}: {i + take_n}          ")
            print(f"loss gen: {recent_loss_com/max(com_counter, 1)}")
            print(f"loss dis: {recent_loss_dis/max(dis_counter, 1)}")
            print(f"accuracy gen: {recent_accuracy_com/max(com_counter, 1)}")
            print(f"accuracy dis: {recent_accuracy_dis/max(dis_counter, 1)}")

            dis_counter = 0
            com_counter = 0

            # reset aggregated loss and accuracy
            recent_loss_com = 0
            recent_loss_dis = 0

            recent_accuracy_com = 0
            recent_accuracy_dis = 0

            # draw the first 16 generated images from the batch
            for n in range(16):
                img = ((np.reshape(generated_images[n], (89, 89, 3))+1)*255/2).astype(int)
                plt.subplot(4, 4, n + 1)
                plt.imshow(img)

            plt.draw()
            plt.savefig(os.path.join(data_path, run_folder, "img_m{}_i{}.png".format(m, i)), dpi=400)
            plt.pause(0.5)
            fig.clear()

            dis_model.save_weights(os.path.join(checkpoint_path, run_folder, "celeba_small_discriminator_m{}_i{}.h5".format(m, i)))
            gen_model.save_weights(os.path.join(checkpoint_path, run_folder, "celeba_small_generator_m{}_i{}.h5".format(m, i)))

            np.save(os.path.join(data_path, run_folder, "loss_hist_celeba_small.npy"), np.array(loss_hist))